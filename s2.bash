#USER-INPUT, CALCULATE AGE

read -p "Enter your Last Name: " LNAME
read -p "Enter your First Name: " FNAME
read -p "Enter your birth month (Ex. 9) " MONTH
read -p "Enter your birth day (Ex. 17) " DAY
read -p "Enter your birth year (Ex. 1999) : " YEAR

PRESENTYEAR=$(date +'%Y')
PRESENTMONTH=$(date +'%m')
PRESENTDAY=$(date +'%d')

REMAININGMONTHS=$(($PRESENTMONTH - $MONTH))

if [ $MONTH -lt $PRESENTMONTH ] || [ $DAY -lt $PRESENTDAY ]; then
    AGER=$(($PRESENTYEAR - $YEAR))
    echo "Hello, $FNAME $LNAME! You're $AGER years old!"
else
    AGEN=$(($PRESENTYEAR - $YEAR -1))
    echo "Hello, $FNAME $LNAME! You're $AGEN years old!"
fi


